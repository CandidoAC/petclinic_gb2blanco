package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTest {
	@Autowired
	private PetRepository petRepository;
	
	@Autowired
	private OwnerRepository ownerRepository;
	
	private Owner owner;
	private Pet pet;
	
	
	@Before
	public void init() {
			owner = new Owner();
			owner.setFirstName("Candido");
			owner.setLastName("Aguilar");
			owner.setAddress("110 N. Lake St.");
			owner.setCity("Madison");
			owner.setTelephone("651786900");	
			ownerRepository.save(owner);
			
			pet=new Pet();
			pet.setName("pipo");
			pet.setOwner(owner);
			List<PetType> PetTypes=petRepository.findPetTypes();
			pet.setType(PetTypes.get(0));
			pet.setBirthDate(LocalDate.now());
			pet.setComments("It's so happy");
			pet.setWeight((float) 40.5);
			petRepository.save(pet);
			pet=new Pet();
			pet.setName("Tommy");
			pet.setOwner(owner);
			pet.setType(PetTypes.get(1));
			pet.setBirthDate(LocalDate.now());
			pet.setComments("It's so kind");
			pet.setWeight((float) 30.5);
			petRepository.save(pet);
			pet=new Pet();
			pet.setName("Boby");
			pet.setOwner(owner);
			pet.setType(PetTypes.get(1));
			pet.setBirthDate(LocalDate.now());
			pet.setComments("It's so kind");
			pet.setWeight((float) 52.3);
			petRepository.save(pet);
	}
	
	@Test
	@Transactional
	public void testPetDelete() {
		assertNotNull(this.petRepository.findById(pet.getId()));
		this.petRepository.delete(pet);
		assertNull(this.petRepository.findById(pet.getId()));	
	}
	
	@Test
	public void testFindPets(){
		List<Pet> listPet=(List<Pet>) this.petRepository.findPetsByName("");
		assertTrue(listPet.size()==pet.getId());
		listPet=(List<Pet>) this.petRepository.findPetsByName("Coco");
		assertTrue(listPet.size()==0);
		listPet=(List<Pet>) this.petRepository.findPetsByName("Tommy");
		assertTrue(listPet.size()==1);
		assertEquals(listPet.get(0).getName(),"Tommy");
		assertEquals(listPet.get(0).getOwner(),owner);
		
	}

}
