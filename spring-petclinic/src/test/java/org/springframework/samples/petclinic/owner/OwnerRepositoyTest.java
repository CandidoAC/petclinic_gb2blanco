package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoyTest {
	
	@Autowired
	private OwnerRepository ownerRepository;
	
	private Owner owner;

	@Before
	public void init() {
			owner = new Owner();
			owner.setFirstName("Candido");
			owner.setLastName("Aguilar");
			owner.setAddress("110 N. Lake St.");
			owner.setCity("Madison");
			owner.setTelephone("651786900");	
			
			this.ownerRepository.save(owner);
	}
	
	@Test
	@Transactional
	public void testDelete() {
		assertNotNull(this.ownerRepository.findById(owner.getId()));
		this.ownerRepository.deleteById(owner.getId());
		assertNull(this.ownerRepository.findById(owner.getId()));
		
	}
		

}