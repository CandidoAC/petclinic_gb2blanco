package org.springframework.samples.petclinic.vet;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class NR2Test {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
	driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testNR2() throws Exception {
    driver.get("http://localhost:8180/");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Find pets'])[1]/following::span[2]")).click();
    driver.findElement(By.linkText("Add Vet")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Pablo");
    driver.findElement(By.id("lastName")).click();
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("cf");
    // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=specialties | label=radiology]]
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::option[2]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Veterinarians'])[2]/following::th[1]")).click();
    assertEquals("Name", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Veterinarians'])[2]/following::th[1]")).getText());
    assertEquals("Pablo cf", driver.findElement(By.linkText("Pablo cf")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::th[1]")).click();
    assertEquals("Specialties", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::th[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pablo cf'])[1]/following::td[1]")).click();
    assertEquals("radiology", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pablo cf'])[1]/following::span[1]")).getText());
    driver.findElement(By.linkText("Pablo cf")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::h2[1]")).click();
    assertEquals("Vet Information", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::h2[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet Information'])[1]/following::th[1]")).click();
    assertEquals("Name", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet Information'])[1]/following::th[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::td[1]")).click();
    assertEquals("Pablo cf", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::b[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pablo cf'])[1]/following::th[1]")).click();
    assertEquals("Specialties", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pablo cf'])[1]/following::th[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::td[1]")).click();
    assertEquals("radiology", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::span[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::span[7]")).click();
    driver.close();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
