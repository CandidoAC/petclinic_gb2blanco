package org.springframework.samples.petclinic.vet;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import javax.print.attribute.HashAttributeSet;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Test class for the {@link VetController}
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VetController.class)
public class VetControllerTests {

    @Autowired
    private MockMvc mockMvc;

	@MockBean
	private VetRepository vets;

	private Vet helen = new Vet();
	private Vet james = new Vet();
	
    @Before
    public void setup() {
        james.setFirstName("James");
        james.setLastName("Carter");
        james.setId(1);
        
        helen.setFirstName("Helen");
        helen.setLastName("Leary");
        helen.setId(2);
        Specialty radiology = new Specialty();
        radiology.setId(1);
        radiology.setName("radiology");
        helen.addSpecialty(radiology);
        
        Specialty surgery = new Specialty();
        radiology.setId(2);
        radiology.setName("surgery");
        james.addSpecialty(surgery);
        given(this.vets.findAll()).willReturn(Lists.newArrayList(james, helen));
		given(this.vets.findById(1)).willReturn(james);
		given(this.vets.findById(2)).willReturn(helen);

    }
    
    @Test
	public void testShowVetListHtml() throws Exception {
		mockMvc.perform(get("/vets.html")).andExpect(status().isOk()).andExpect(model().attributeExists("vets"))
				.andExpect(view().name("vets/vetList"));
	}
     
    @Test
    public void testShowVetSpecialties() throws Exception {
        mockMvc.perform(get("/vets/vetSpecialties"))
            .andExpect(status().isOk())
            .andExpect(view().name("vets/vetSpecialties"))
            ;
    }
    @Test
    public void testShowInitCreationForm() throws Exception {
    	
        mockMvc.perform(get("/vets/new"))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("vet"))
            .andExpect(view().name("vets/createOrUpdateVetForm"));
    }
    
    @Test
    public void testProcessCreationFormHasErrors() throws Exception {
        mockMvc.perform(get("/vets/new"))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("vet"))
            .andExpect(view().name("vets/createOrUpdateVetForm"));
    }
    
    @Test
    public void testProcessCreationFormNoErrors() throws Exception {
        mockMvc.perform(post("/vets/new")
        		.param("FirstName", "Manuel")
        		.param("LastName", "Garcia")
        		.param("vet_id", "99")
        		.param("specialties", "1")
        		)
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/vets.html"));
    }
    
    @Test
	public void testShowVet() throws Exception {
		Collection<Specialty> vetSpec = helen.getSpecialties();
		mockMvc.perform(get("/vets/{vetId}", 2)).andExpect(status().isOk())
				.andExpect(model().attribute("vet", hasProperty("lastName", is("Leary"))))
				.andExpect(model().attribute("vet", hasProperty("firstName", is("Helen"))))
				.andExpect(model().attribute("vet", hasProperty("specialties", is(vetSpec))))
				.andExpect(view().name("vets/vetDetails"));

	}
	
    @Test
    public void testShowResourcesVetList() throws Exception {
        ResultActions actions = mockMvc.perform(get("/vets")
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        actions.andExpect(content().contentType("application/json;charset=UTF-8"))
            .andExpect(jsonPath("$.vetList[0].id").value(1));
    }

}
