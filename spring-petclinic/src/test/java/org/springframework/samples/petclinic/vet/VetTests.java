/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Dave Syer
 *
 */
public class VetTests {

	public static Vet vet;
	public static Vet vet2;

	@BeforeClass
	public static void initClass() {
		vet = new Vet();
		vet.setFirstName("Pablo");
		vet.setLastName("CF");
		vet.setHomeVisits(false);
	}

	@Test
	public void testVet() {
		assertNotNull(vet);				
	}

	@Test
	public void testVet2() {
		assertNull(vet2);				
	}

	@Test
	public void testSerialization() {
		Vet vet = new Vet();
		vet.setFirstName("Zaphod");
		vet.setLastName("Beeblebrox");
		vet.setId(123);
		Vet other = (Vet) SerializationUtils
				.deserialize(SerializationUtils.serialize(vet));
		assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
		assertThat(other.getLastName()).isEqualTo(vet.getLastName());
		assertThat(other.getId()).isEqualTo(vet.getId());
	}

	@Test
	public void testHomeVisits() {
		assertNotNull(vet.getHomeVisits());
		assertFalse(vet.getHomeVisits());
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits());
	}

	@AfterClass
	public static void finishClass() {
		vet = null;
	}
}
