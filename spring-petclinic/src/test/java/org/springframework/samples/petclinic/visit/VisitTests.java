/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class VisitTests {
	
	public static Vet vet1;
	public static Vet vet2;
	public static Visit visit;
	public static Pet pet;
	
	@Before
	//This method is called before executing this suite
	public void initClass() {
		LocalDate today = LocalDate.now();
		//PetType raza = PetTyp
		vet1 = new Vet();
		vet1.setFirstName("Manuel");
		vet1.setLastName("Garcia");
		vet1.setHomeVisits(true);
		vet2 = new Vet();
		vet2.setFirstName("Veterinario");
		vet2.setLastName("Dos");
		vet2.setHomeVisits(true);
		pet = new Pet();
		pet.setId(9000);
		pet.setName("Pipo");
		//pet.setType("Dog");
		pet.setWeight(8.5f);
		pet.setComments("Está muy sano");
		pet.setBirthDate(today);
		visit = new Visit();
		visit.setDate(today);
		visit.setDescription("Visita de prueba generada en con test de JUnit");
		visit.setId(9999);
		visit.setPetId(9000);
		visit.setVet(vet1);
	}
	
	
	@Test
	public void testVet() {
		assertNotNull(vet1);				
	}
	
	@Test
	public void testVet2() {
		assertNotNull(vet2);				
	}
	
	@Test
	public void testPet() {
		assertNotNull(pet);				
	}
	
	@Test
	public void testVisit() {
		assertNotNull(visit);
	}
	
	@Test
	public void testVisitId() {
		assertNotNull(visit.getId());
		assertFalse(visit.getId() == 123);
		assertTrue(visit.getId() == 9999);
		assertFalse(visit.getPetId() == 1111);
		assertTrue(visit.getPetId() == 9000);
		assertFalse(visit.getDescription() == "Error.");
		assertTrue(visit.getDescription() == "Visita de prueba generada en con test de JUnit");
	}
	
	
	@Test
	public void testAsignarVisit() {
		assertNotNull(visit.getVet());
		assertFalse(visit.getVet().getHomeVisits() == false);
		assertTrue(visit.getVet().getHomeVisits() == true);
		assertNotEquals(visit.getVet().getFirstName(), "Julio");
		assertEquals(visit.getVet().getFirstName(), "Manuel");
		assertNotEquals(visit.getVet().getLastName(), "Dos");
		assertEquals(visit.getVet().getLastName(), "Garcia");
	}

	@Test
	public void testCambiarVisitVet() {
		visit.setVet(vet2);
		assertNotNull(visit.getVet());
		assertFalse(visit.getVet().getHomeVisits() == false);
		assertTrue(visit.getVet().getHomeVisits() == true);
		assertNotEquals(visit.getVet().getFirstName(), "Manuel");
		assertEquals(visit.getVet().getFirstName(), "Veterinario");
		assertNotEquals(visit.getVet().getLastName(), "Garcia");
		assertEquals(visit.getVet().getLastName(), "Dos");
	}
	
	@Test
	public void testHomeVisits() {
		assertNotNull(vet1.getHomeVisits());
		assertTrue(vet1.getHomeVisits());
		assertNotNull(vet2.getHomeVisits());
		assertTrue(vet2.getHomeVisits());
	}
	
    @Test
    public void testSerialization() {
    	Vet vet3 = new  Vet();
        vet3.setFirstName("Zaphod");
        vet3.setLastName("Beeblebrox");
        vet3.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet3));
        assertThat(other.getFirstName()).isEqualTo(vet3.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet3.getLastName());
        assertThat(other.getId()).isEqualTo(vet3.getId());
    }
    
	@Ignore
	//This test is not executed
	public void ignore() {
		vet1.setFirstName("JuanMa");
	}
    
    @After
    //This test is executed after each test created in this suite
    public void finish() {
    	vet1.setHomeVisits(true);
    }
    
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vet1 = null;
    }
    

}