
/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class PetTest {
	
	public static Pet pet;
	public static Pet pet2;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		pet = new Pet();
		pet.setName("Tommmy");
		pet.setComments("Is a good boy");
		pet.setWeight(8.5f);
	}
	
		
	@Test
	public void testVet() {
		assertNotNull(pet);				
	}
	
	@Test
	public void testVet2() {
		assertNull(pet2);				
	}
	
	@Test
	public void testComments() {
		assertNotNull(pet.getComments());
		assertNotEquals(pet.getComments(), "Is a boy");
		assertEquals(pet.getComments(), "Is a good boy");		
	}
	
	@Test
	public void testWeight() {
		assertNotNull(pet.getWeight());
		assertEquals(8.5, pet.getWeight(), 0.0f);
		assertNotEquals(8.3, pet.getWeight(),0.0f);		
	}

    
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	pet = null;
    }
    

}