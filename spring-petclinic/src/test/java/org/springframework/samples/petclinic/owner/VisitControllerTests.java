package org.springframework.samples.petclinic.owner;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.time.LocalDate;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.VisitController;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.vet.*;

/**
 * Test class for {@link VisitController}
 *
 * @author Colin But
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VisitController.class)
public class VisitControllerTests {

    private static final int TEST_PET_ID = 1;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VisitRepository visits;

    @MockBean
    private PetRepository pets;
    
    @MockBean
    private VetRepository vets;
    
	private Owner owner;
    
    private Pet pet;
    
    private Vet vet;
    
    private Visit visit1,visit2;

    @Before
    public void init() {
        given(this.pets.findById(TEST_PET_ID)).willReturn(new Pet());
        
        owner = new Owner();
		owner.setFirstName("Pablo");
		owner.setId(5);
		owner.setLastName("cf");
		owner.setAddress("95 N. Lake St.");
		owner.setCity("Caceres");
		owner.setTelephone("658457896");	
		
		vet = new Vet();
		vet.setFirstName("Car");
		vet.setLastName("dc");
		vet.setHomeVisits(true);
		Specialty radiology = new Specialty();
        radiology.setId(1);
        radiology.setName("radiology");
        vet.addSpecialty(radiology);
		
        pet=new Pet();
		pet.setName("Pipo");
		pet.setId(1);
		PetType type=new PetType();
		type.setId(1);
		type.setName("dog");
		pet.setType(type);
		pet.setBirthDate(LocalDate.now());
		pet.setComments("It's so happy");
		pet.setWeight(40.5f);
		
        visit1 = new Visit();
        visit1.setId(1);
        visit1.setDate(LocalDate.now());
        visit1.setDescription("Descripcion visita1");
        
        
        visit2 = new Visit();
        visit2.setId(2);
        visit2.setDate(LocalDate.now());
        visit2.setDescription("Descripcion visita2");
        
        pet.addVisit(visit1);
        pet.addVisit(visit2);
        vet.addVisit(visit1);
        vet.addVisit(visit2);
        owner.addPet(pet);
        given(this.visits.findByPetId(1)).willReturn(Lists.newArrayList(visit1,visit2));
        given(this.visits.findById(1)).willReturn(visit1);
        given(this.visits.findById(2)).willReturn(visit2);
    }

    @Test
    public void testInitNewVisitForm() throws Exception {
        mockMvc.perform(get("/owners/*/pets/{petId}/visits/new", TEST_PET_ID))
            .andExpect(status().isOk())
            .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }

    @Test
    public void testProcessNewVisitFormSuccess() throws Exception {
        mockMvc.perform(post("/owners/*/pets/{petId}/visits/new", TEST_PET_ID)
            .param("name", "George")
            .param("description", "Visit Description")
        )
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/owners/{ownerId}"));
    }

    @Test
    public void testProcessNewVisitFormHasErrors() throws Exception {
        mockMvc.perform(post("/owners/*/pets/{petId}/visits/new", TEST_PET_ID)
            .param("name", "George")
        )
            .andExpect(model().attributeHasErrors("visit"))
            .andExpect(status().isOk())
            .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }
    
    @Test
    public void testInitEditVisitForm() throws Exception {
        mockMvc.perform(get("/owners/{ownerId}/pets/{petId}/visits/{visitId}/edit",owner.getId(),pet.getId(),visit2.getId()))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("visit"))
            .andExpect(view().name("pets/createOrUpdateVisitForm"));
    	
    }

    @Test
    public void testProcessEditVisitFormSuccess() throws Exception {
        mockMvc.perform(post("/owners/{ownerId}/pets/{petId}/visits/{visitId}/edit", owner.getId(),pet.getId(),visit2.getId())
        	.param("date", "2018-12-21")
        	.param("description", "Nueva descripcion de visit2")
        )
        .andExpect(status().is3xxRedirection())
        .andExpect(view().name("redirect:/owners/{ownerId}"));
    }
    
    @Test
    public void testProcessEditVisitFormHasErrors() throws Exception {
        mockMvc.perform(post("/owners/{ownerId}/pets/{petId}/visits/{visitId}/edit", owner.getId(),pet.getId(),visit1.getId())
            .param("date", "2018-12-21")
        )
            .andExpect(status().is(400));
    }

}
